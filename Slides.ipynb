{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# CellProfiler Tutorial  \n",
    "\n",
    "## *Hands-on Workshop on High-Throughput Microscopy*\n",
    "## Lisbon 2017\n",
    "\n",
    "### Volker Hilsenstein\n",
    "<img src=\"./Screenshots/EMBLlogo.gif\" alt=\"\" width=200px>\n",
    "**Advanced Light Microscopy Facility**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#  CellProfiler\n",
    "## A free, open-source software for high throughput microscopy image analysis\n",
    "\n",
    "* Free, open-source software from the Broad Institute\n",
    "* Active community\n",
    "* Well documented\n",
    "<img src=\"./Screenshots/CPWebsite.png\" alt=\"\" width=600px>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Where does CellProfiler fit into your high-throughput microscopy workflow ?\n",
    "\n",
    "## Cellprofiler helps you to transform images into measurements\n",
    "<img src=\"./Screenshots/HTMPipeline.png\" alt=\"\" width=500px>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# From images to measurements (numbers)\n",
    "\n",
    "## What features (numbers) can you extract from a microscope image ? \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "\n",
    "<center><img src=\"./Screenshots/Phenotypes.png\" alt=\"\" width=1000px></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "* **Intensity** - (pixel intensity -> what does it mean physically ?)\n",
    "* **Cell count**\n",
    "* **Size of objects** (cells, cell organelles)\n",
    "* **Shape**\n",
    "* **Texture**\n",
    "* **Location and spatial relationships** (distances between organelles, clustering)\n",
    "* **Dynamics for time-lapse imaging** (cell mobility, wound closing, decay rates)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Why use CellProfiler ? \n",
    "## ... could we just use Fiji / ImageJ ?\n",
    "\n",
    "There are many image analysis packages. Among biologists ImageJ/Fiji is very popular,\n",
    "so why not just create an ImageJ macro for your analysis? \n",
    "\n",
    "High throughput image analysis requires more than just macros for image analysis.\n",
    "\n",
    "Useful features for high througput image analysis is support for:\n",
    "\n",
    "* **Pipeline building**\n",
    "* **Batch processing**\n",
    "* **Metadata handling**\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Pipeline building\n",
    "\n",
    "A typical image analysis workflow typically consists of several individual image processing\n",
    "and segmentation steps.\n",
    "\n",
    "* To build a pipeline we need to **pass data between different modules**. The data can for example be images, segmentation regions, measurements or file names.\n",
    "* **ImageJ/Fiji** has many powerful macros but they often are not designed to work together seamlessly. \n",
    "* **CellProfiler** is optimized for building workflows, as images, regions ad measurements that are generated at any step of the pipeline are automatically available in subsequent modules."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Batch Processing\n",
    "\n",
    "\n",
    "Once we have established an image analysis workflow we want to apply it to many images. \n",
    "\n",
    "* Find image files matching certain naming patterns (** regular expression matching **)\n",
    "* Group images that belong together (** image sets **)\n",
    "* Split work between different machines (** cluster / multi-processing **)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Metadata handling\n",
    "\n",
    "**Metadata** is generated at various steps of the high-throughput-screening pipeline\n",
    "<center><img src=\"./Screenshots/WorkflowMetadata.png\" alt=\"\" width=280px></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Example Metadata:\n",
    "    \n",
    "**Data handling**: filename, timestamp <br>\n",
    "**Cell culture**: cell type, batch number, time <br>\n",
    "**Treatment**: e.g. which siRNA or reagent <br>\n",
    "**Plate location:**: plate name, well number, sub position <br> \n",
    "**Imaging**: fluorescence channel, microscope settings <br>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# CellProfiler facilitates metadata handling\n",
    "\n",
    "\n",
    "1. You can extract metadata from **file names** and **folder names**.\n",
    "2. The **Metadata** can be stored as a column in the table of measurements generated by CellProfiler.\n",
    "3. If done properly this will allow you to link individual data points back to the metadata images\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Alternatives to CellProfiler\n",
    "\n",
    "### Knime\n",
    "\n",
    "**Knime** is a graphical programming language that contains many image analysis functions. It brings both its own image analysis functions as well as building blocks that let you call ImageJ macros.\n",
    "\n",
    "<center><img src=\"./Screenshots/KnimeWebpage.png\" alt=\"\" width=600px></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# More Alternatives to CellProfiler\n",
    "\n",
    "* Commercial software that comes with your microscope\n",
    "* Program your own image analysis workflow using programming languages such as **Matlab** or **Python**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Interactive - Building a CellProfiler Pipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Start CellProfiler"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# The Cellprofiler GUI\n",
    "\n",
    "\n",
    "* **Pipeline panel** (left, split into _Input Modules_ and _Analysis Modules_) \n",
    "* **Module settings panel** (right) each module has a number of parameters. If you highlight a module in the pipeline panel its settings can be inspected and edited in this panel.\n",
    "* **Menu bar**\n",
    "* **Add modules**\n",
    "* **Analyze Images** or **Start Test Run**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "\n",
    "# A typical image analysis workflow for cell-based assays\n",
    "\n",
    "* **Load image sets:** (multiple channels), **extract metadata**\n",
    "* **Pre-process images:** (de-noising, illumination correction, feature-enhancement)\n",
    "* **Segment nuclei: ** thresholding and splitting clumps. **Need a nuclear marker**, e.g. DAPI\n",
    "* **Segment cytoplasm: ** using dilation of nuclei, watershed or seeded region growing. **Need a suitable marker**\n",
    "* **Calculate measurements on nuclei and cytoplasm: ** size, shape, intensity -> need for quality control later. \n",
    "* **Assay-specific analysis: ** e.g. segment an organelle, measure intensity ratio\n",
    "* **Export Measurements**\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "An example:\n",
    "\n",
    "<center><img src=\"./Screenshots/overall_workflow.png\" alt=\"\" width=800px></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Importing Images\n",
    "* At the top of the pipeline on the left select **`Images`** under **`Input Modules`**.\n",
    "* Drop the test images from the folder of the harddrive onto the grey file list area. \n",
    "* You can apply various filters, e.g. restricting the files to `tif` files.\n",
    "![](./Screenshots/DropFiles.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Metadata extraction\n",
    "\n",
    "* You can extract metadata from **file names** and **folder names** using so-called **regular expressions**.\n",
    "* Define the **regular expressions** in the `Metadata` section under `Input Modules` you can define regular expressions.\n",
    "\n",
    "<center><img src=\"./Screenshots/MetadataEditor.png\" alt=\"\" width=800px></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Tip: Mastering regular expressions\n",
    "\n",
    "* Regular expressions are **very powerful but take some practice** to master.\n",
    "* CellProfiler's built-in regular expression editor is good, but [**regexp101.com**](http://regex101.com/) is **better**.\n",
    "\n",
    "<center><img src=\"./Screenshots/regex101.png\" alt=\"\" width=800px></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Give meaningful names to images\n",
    "* Typically you will have multiple images that need to be analyzed together, e.g. different channels.\n",
    "* In the **`Names and Types`** section you can assign names to your images that will be used throughout the pipeline. For example you could assing the name DAPI to channel 0. \n",
    "\n",
    "<center><img src=\"./Screenshots/NamesAndTypes.png\" alt=\"\" width=600px></center>\n",
    "\n",
    "## Group images\n",
    "* The **`Groups`** module lets you group images, for example by well position.  \n",
    "* **Grouping images** tells CellProfiler that several image sets have to be analyzed together, e.g. when doing **cell tracking**.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Add a module, run a pipeline in test mode, look at the images\n",
    "\n",
    "* Click on **`+`** to add a module. \n",
    "* Add the module **`ImageMath`**, select an image\n",
    "* Start test mode\n",
    "* Step through the pipeline\n",
    "* Observe image intensities in the pipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Image Pre-Processing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Image Preprocessing\n",
    "\n",
    "The first step in most image analysis pipeline is some preprocessing of the raw\n",
    "input images.\n",
    "You may want to add preprocessing steps for:\n",
    "\n",
    "* Illumination correction (**`CorrectIllumination`**)\n",
    "* Noise reduction (**`Smooth`**)\n",
    "* Background subtraction (**`ImageMath`**)\n",
    "* Feature enhancement"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "\n",
    "## Reduce noise with Gaussian smoothing\n",
    "* add the **`Smooth`** module\n",
    "* select Gaussian smoothing\n",
    "* try the smoothing filter with various artifact radii\n",
    "<center><img src=\"./Screenshots/Smooth.png\" alt=\"\" width=600px></center>\n",
    "* inspect the intensities in the background before and after smoothing (hover with the mouse)\n",
    "* observe what happens to the object boundaries"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Use `ImageMath` to remove fluorescent background\n",
    "\n",
    "* get an idea about the intensity values (they are probably different from what you expect) by hovering\n",
    "* subtract a number"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Segmentation of Cell regions\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Segment Nuclei\n",
    "\n",
    "**Finding cell nuclei** is typically the **first segmentation step** when analysing fluoresence images of cells. \n",
    "<p>\n",
    "**Hopefully, you included a fluorescent marker for the nuclei in your assay**\n",
    "<p>\n",
    "Nuclei are a good starting point for segmentation:\n",
    "* contiguous objects\n",
    "* mostly spatially separated (because they are surrounded by cytoplasm)\n",
    "* segmentation can often be done by simple thresholding\n",
    "\n",
    "<center><img src=\"./Screenshots/nuclei_neg9.png\" alt=\"\" width=200px></center>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "\n",
    "# Perform nuclei segmentation with the **`IdentifyPrimaryObjects`** module\n",
    "\n",
    "Add the **`IdentifyPrimaryObjects`** module to the pipeline and select the input image. \n",
    "<p>\n",
    "<center><img src=\"./Screenshots/IdentifyPrimaryObjects.png\" alt=\"\" width=800px></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Experiment with different settings in **`IdentifyPrimaryObjects`**\n",
    "* Run the module with the default parameters. \n",
    "* Try using different segmentation methods (Otsu, Background, Robustbackground). \n",
    "* Try different size ranges for the objects.\n",
    "* Discard/retain objects touching the image border.\n",
    "* Try different options for splitting objects.  \n",
    "* Go back to the `Smooth` module and try different Gaussian blur sizes. How does the smoothing change the segmentation results? "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Perform segmentation of the cytomplasm with the **`IdentifySecondaryObjects`** module\n",
    "\n",
    "\n",
    "CellProfiler implements various image analsysis methods for identifying the cytoplasm. \n",
    "<p>\n",
    "\n",
    "* Is your sample labelled with a **fluorescent marker for the cytoplasm** or a **cell delineation marker** (such as CellMask)? Then you can use the **`Watershed`** or **`Propagation`** methods.\n",
    "* Without a fluorescent marker for the cytoplasm you should use methods such as **`Distance-N`**. The segmentation results may not be quite as accurate but you are less likely to introduce a bias."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Filtering and Quality controls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "\n",
    "## Apply quality controls to remove data points that should not be quantified.\n",
    "\n",
    "* Remove whole plates/wells/images with technical problems \n",
    "* Remove cells that can't be used for quantification\n",
    "<p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## You have a choice: filter in CellProfiler or filter downstream during the statistical analysis\n",
    "\n",
    "In either case you need to calculate the metrics that you need for quality control in the pipeline !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Reject out-of-focus images\n",
    "\n",
    "It is quite common that a few of your images will end up out of focus, sometimes this is due to plate bottoms with uneven thickness or failures of the autofocus system.\n",
    "<p> \n",
    "**Quantify how well focused an image is !**\n",
    "<p>\n",
    "One suitable measure is the power-log-log slope that CellProfiler can calculate as part of the pipeline.\n",
    "<p>\n",
    "<center><img src=\"./Screenshots/PLLS_Tischi.png\" alt=\"\" width=400px></center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Check for saturation, and reject objects/images that are oversaturated\n",
    "\n",
    "\n",
    "For an image with `8` bits per pixel, the maximum greyvalue for a pixel is `255`. <p>\n",
    "\n",
    "If a pixel has the **maximum greyvalue** for the given bit depth it is probably **saturated**. The true intensity is probably greater than `255` but cannot be recorded by the camera using the given settings. \n",
    "<p>\n",
    "If you have images/objects with many saturated pixels, you **cannot quantify intensity**.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Export analysis results for downstream analysis\n",
    "\n",
    "* Save measurements as tables using **`ExportToSpreadsheet`**\n",
    "* Create and save images that show segmentation masks and measurement results. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "* Add the **`ExportToSpreadsheet`** module\n",
    "\n",
    "<center><img src=\"./Screenshots/ExportToSpreadSheet.png\" alt=\"\" width=400px></center>\n",
    "* Choose a suitable output folder. You can create folder names based on extracted metadata fields (right-click).\n",
    "* Add metadata columns.\n",
    "* Select the measurements to export. \n",
    "* Calculate mean, median and standard deviation for each measurement."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Click on **`Press button to select measurements`** to bring up the following dialog\n",
    "<center><img src=\"./Screenshots/ExportSelectMeasurements.png\" alt=\"\" width=300px></center>\n",
    "* Select what you want to export. \n",
    "* Be sure to include image file names and path names as well as metadata.\n",
    "* Run a batch analysis \n",
    "* Inspect the output tables using Excel \n",
    "\n",
    "**Caution Excel users:**  Depending on your language settings the comma (column separator in `.csv`-file) might also be the decimal separator. This can cause problems."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# References\n",
    "\n",
    "Online material:\n",
    "\n",
    "* CellProfiler Tutorials: http://cellprofiler.org/tutorials/\n",
    "* https://git.embl.de/hilsenst/CellProfilerCourse-HTM\n",
    "\n",
    "Papers:\n",
    "\n",
    "* [Bray M.A., Carpenter, A. _Advanced Assay Development Guidelines for Image- Based High Content Screening and Analysis_ 2012](https://www.ncbi.nlm.nih.gov/books/NBK126174/)\n",
    "* [Verbjorn L, Carpenter, A. _Introduction to the Quantitative Analysis of Two- Dimensional Fluorescence Microscopy Images for Cell- Based Screening_, PLOS Computational Biology 2009](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1000603)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Acknowledgements\n",
    "\n",
    "* **Cellprofiler Team** headed by **Anne Carpenter**, Broad Institute\n",
    "* Team of the EMBL Advanced Light microsopy facility, in particular Christian Tischer\n",
    "<img src=\"./Screenshots/ALMFTeam.jpg\" alt=\"\" width=550px>\n",
    "* ALMF Visitors and Users"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "tools for presentation: \n",
    "    \n",
    "* [sketchtoy](http://sketchtoy.com/68222320)\n",
    "* [sktechpad](https://sketch.io/sketchpad/)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  },
  "livereveal": {
   "start_slideshow_at": "selected",
   "theme": "Simple",
   "transition": "fade"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
