# Alternatives to CellProfiler

## KNIME

KNIME is a framework for data analysis that lets you create workflows using 
a graphical programming interface. You can combine nodes that wrap functions from many
different software packages, including ImageJ macros, R and Python functions as well as database
access. Like Cellprofiler, KNIME is open source. Due to the availability of statistics modules
you can also include your statistical analysis in KNIME in addition to taking measurements
from images. While KNIME is probably more flexible (for example, your analysis pipeline can have
several different branches) it is also more complex. 

![screenshot KNIME webpage](../Screenshots/KnimeWebpage.png)

## Commercial Solutions

A number of microscope manufacturers offer powerful software packages together with 
their screening microscopes. Often these software packages have a nice interface and due
to the integration one can do image acquisition, image analysis and statistical
analysis in the same software. Due to the tight integration you do not have to worry about
file format conversion, you can link your data points back to your original images and you 
can sometimes go back from the images to the position on the stage (and recapture the image). 
The drawback is that these software packages often require an expensive license, they are closed-source black box
systems (you don't know what the algorithm is doing) and you can't extend them yourself if you are
missing some important functionality. If you have microscopes from different manufacturers, you 
have to use different software packages.

[Proceed to **Importing images, extracting metadata**](ImportingImages.md)  
[Back to **Overview**](OverviewGettingStarted.md)






