# Filter cells based on measurements

In the initial stages of the image analysis pipeline you typically want to 
find all the nuclei / cells. However, your final analysis will sometimes only include
a subset of cells. 

# Select a subset of cells ~work-along
* Add the `FilterObjects` module. 
* Select an object, e.g. Cells and filter based on intensity or shape. You can also
create filters that combines different measurements.
![](../Screenshots/FilterObjects.png)

# Typical filter scenario - Quality Control 

* When your assay readout is based on fluorescence intensity, you should exclude
cells that are overexpsosed. Try to filter out cells that contain more than 10 saturated pixels?
Work out what floating point value in CellProfiler corresponds to a saturated intensity. Keep track
of how many overexposed cells were discarded to prevent bias. ~self-study
* Try to exclude mitotic and dead cells from your analyis. Also see [Additional Refinemens](AdditionalMaterial.md) ~self-study


[Proceed to **Export Results**](ExportResults.md)  
[Back to **Overview**](OverviewGettingStarted.md)
