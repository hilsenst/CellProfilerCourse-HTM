# Refining the image analysis pipeline

## Overlay and save segmentation results ~self-study
* Add the `OverlayOutlines` module to your pipeline,
* Go back to the `IdentifyPrimaryObjects` and `IdentifySecondaryObjects` modules and make sure you check the option to retain the outlines of the identified objects. Name the outlines.
* In the `OverlayOutlines` module, select the image to use for overlaying the outline
* Using Test Mode, adjust line width and colors until you are happy with the results
* Add the `SaveImages` module to save the overlay image. 
* You can also use `DisplayDataOnImage` on your overlay image.

## Re-use metadata for constructing output file/folder names ~self-study

In various CellProfiler modules, you need to specify an output folder or filename, e.g. in `ExportToSpreadsheet` or in `SaveImages`. 
Rather than giving a fixed filename, you can construct a new file/foldername from the metadata you extracted in the input module.
* In `SaveImages` select `Single name` as the method to construct the filename. Right-click into the file name field. You will see a menu in which you can select from the metadata fields extracted in `Input Modules`.


[Proceed to **Additional Refinements**](AdditionalMaterial.md)  
[Back to **Overview**](OverviewGettingStarted.md)
