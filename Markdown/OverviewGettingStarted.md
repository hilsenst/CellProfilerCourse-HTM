# Overview

The aim of this tutorial is to get you started with CellProfiler for analysing high-throughput microscopy image data.
The focus is on how you can integrate CellProfiler into your high-throughput screening workflow, based on our own experience in a microscopy core facility.
This tutorial **is not** on the foundations and particulars of image analysis.

The tutorial is organized as follows sections:

1. Participants background in image analysis and software packages used previously ~discussion
1. [Where does CellProfiler fit in a HTM screening workflow](HTMWorkflow.md) ~presentation
1. [Alternatives to Cellprofiler](Alternatives.md) ~presentation 
1. Building a simple analysis pipeline
    * Starting cellprofiler, loading a pipeline, elements of the user interface ~presentation
    * Adding modules ~presentation
    * Pipeline overview ~presentation
    * [Importing images, extracting metadata](ImportingImages.md) ~work-along
    * [Preprocessing](Preprocessing.md) ~work-along
    * [Segment Nuclei](IdentifyPrimary.md) ~work-along
    * Using test run to debug your pipeline and tune parameters ~presentation
    * Explore different parameters for segmentation and preprocessing ~self-study
    * [Segment Cytoplasm](IdentifySecondary.md) ~work-along
    * [Take some measurements](TakeMeasurements.md) ~work-along
    * [Filter cells based on measurements](FilterCells.md) ~work-along
    * [Export results](ExportResults.md) ~work-along
1. Run a batch analysis ~work-along
1. Inspect the output tables using Excel. ~work-along 
1. [Refining the image analysis pipeline](Refinements.md) ~self-study
    * Overlay and save segmentation results 
    * Re-use metadata for constructing output file/folder names
1. [Additional Refinements](AdditionalMaterial.md) ~self-study
    * Understand the danger of automated segmentation and put in safeguards
    * Background correction
    * Filter out dead and mitotic cells.
    * Measure image quality to detect out of focus images
1. [Recommended Reading](RecommendedReading.md) ~self-study

# Getting Started

* You will have to download CellProfiler. CellProfiler is developed by Anne Carpenter's group at the Broad Institute and available as open source software. You can download it at http://cellprofiler.org
* You can download this tutorial, including the example images and the CellProfiler example pipelines 
from the git-repository at https://git.embl.de/hilsenst/CellProfilerCourse-HTM by clicking on `Repository` and then `Download Zip`.
* If you can't see the `Download Zip`-button, try to make your browser window wider.
![](../Screenshots/DownloadZip.png)

# Labels
The topics are marked with the following labels

~presentation Content that is delivered as a presentation. Participants can watch and listen and don't need to work along. Not all of the presentation content is in the repository, as some of it is live presentation, not slides.

~discussion Discussion with the participants

~work-along Content is delivered on the projector, participants should work along on their laptops.

~self-study Some exercises that participants can work through