# Recommended Reading / References


Papers:
* [Verbjorn L, Carpenter, A. _Introduction to the Quantitative Analysis of Two- Dimensional Fluorescence Microscopy Images for Cell- Based Screening_, PLOS Computational Biology 2009](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1000603)
* [Bray M.A., Carpenter, A. _Advanced Assay Development Guidelines for Image- Based High Content Screening and Analysis_ 2012](https://www.ncbi.nlm.nih.gov/books/NBK126174/)

Online material:

* CellProfiler Tutorials: http://cellprofiler.org/tutorials/

[Back to **Overview**](OverviewGettingStarted.md)