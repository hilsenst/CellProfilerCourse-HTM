# Taking measurements

## Measure fluorescence intensity ~work-along

![](../Screenshots/MeasureObjectIntensity.png)

## Measure object size and shape ~work-along

![](../Screenshots/MeasureObjectSizeShape.png)

## How to view the measurements ~work-along
The output windows of the `MeasureObject...` modules only show measurement summaries, 
not measurements for individual cells.
To get a better idea about what individual measurements look like, use the 
`DisplayDataOnImage` module. 

* Add the `DisplayDataOnImage` to the pipeline.
* Select a measurement that you are interested in, e.g. nuclear size.
* Select the corresponding image
* Repeat for other measurements, either by changing the module during the test run
or by adding additional `DisplayDataOnImage` modules.



[Proceed to **Filter cells based on measurements**](FilterCells.md)  
[Back to **Overview**](OverviewGettingStarted.md)