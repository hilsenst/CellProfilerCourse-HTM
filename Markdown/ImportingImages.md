# Importing Images into CellProfiler


### Importing Images
* At the top of the pipeline on the left select `Images` under `Input Modules`.
* Drop the test images from the folder of the harddrive onto the grey file list area. 
* You can apply various filters, e.g. restricting the files to `tif` files.
![](../Screenshots/DropFiles.png)

### Metadata extraction
* During a high-throughput-screen you want to keep track of metadata throughout the process. This includes image metadata, such as Z-slice, and channel but can also metadata about plate names, well positions and reagents. 
* At the ALMF we include much of this metadata in the image filename (there are other approaches, in which the metadata is stored in a database)
* Cellprofiler offers the possibility to extract metadata from the file and folder names. The benefit is that in the output table, the metadata are directly stored with image measurements taken by Cellprofiler.   
* In the `Metadata` section under `Input Modules` you can use Python regular expressions to extract the metadata. A dedicated metadata editor helps you by highlighting the extracted fields in color. If you have never worked with Python regular expressions, there is a learning curve. Refer to the help in CellProfiler, or for a full documentation https://docs.python.org/3/library/re.html

![](../Screenshots/MetadataEditor.png)


*Tip:* 

Regular expressions are very powerful but can quickly become very complicated and are difficult to debug. While CellProfiler's regular expression 
editor already provides coloured highlighting of matches, there is a more powerful tool on the web, which helps you develop and test regular expressions: [regexp101.com](http://https://regex101.com/)

![](../Screenshots/regex101.png)


### Name images
* Typically you will have multiple images that need to be analyzed together, e.g. different channels.
* In the `Names and Types` section you can assign names to your images that will be used throughout the pipeline. For example you could assing the name DAPI to channel 0. 
![](../Screenshots/NamesAndTypes.png)

### Group images
* The `Groups` module lets you group images, for example by well position. Use this, if you want to use the cell tracking modules for multi-position timelapse experiments.


[Proceed to **Image Preprocessing**](Preprocessing.md)  
[Back to **Overview**](OverviewGettingStarted.md)