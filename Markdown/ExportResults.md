# Export Measurement Results

At the end of the pipeline, you want to export your measurements to a table 
for further analysis. Note that the ExportModules cannot be used in test mode (therefore a warning 
sign is shown in the pipeline) but only when you run a batch analysis.

## Export to a .csv file for use in R, Excel etc. ~work-along

* Add the `ExportToSpreadsheet` module
![](../Screenshots/ExportToSpreadSheet.png)
* Here, you will have to change many options from their default values, as indicated by the arrows in the screenshot
    * Choose a suitable output folder. You can create folder names based on extracted metadata fields (right-click).
    * Add metadata columns.
    * Select the measurements to export. 
    * Calculate mean, median and standard deviation for each measurement.
* `Press button to select measurements` to bring up the following dialog
![](../Screenshots/ExportSelectMeasurements.png)
* Unfold the hierarchy of measurements and decide what you want to export. Make sure to include image file names and path names as well as metadata.

## Run a batch analysis ~work-along

## Inspect the output tables using Excel ~work-along

**Caution Excel users:** When you are using Mac OS X or Windows with English as the default language,
importing `.csv`-files into Excel is straightforward. If your default language is different from
English and the decimal seperator in your country happens to be a commma not a dot,
you will have to specify the column delimiter and decimal separator when trying to import the `.csv`-file. 

[Proceed to **Refining the image analysis pipeline**](Refinements.md)  
[Back to **Overview**](OverviewGettingStarted.md)

## Export to database

If you are running a SQL database server it is possible to use the `ExportToDatabase` module instead of `ExportToSpreadsheet`.
This will directly save the results in your database.

[Proceed to **Refining the image analysis pipeline**](Refinements.md)  
[Back to **Overview**](OverviewGettingStarted.md)
