# Additional refinements. ~self-study

## Understand the danger of automated segmentation and put in safeguards
* Example: Use an image that does not contain any cells but background noise. Alternatively, use the `Crop` module to restrict one of your test images to an area without cells.
* Use adaptive thresholding (`IdentifyPrimaryObjects`) using the Otsu method on the cropped image. ![](../Screenshots/IdentifyPrimaryObjects_CropToNoise.png)
* Observe what is happening. Do you understand what is going on? 
* How can you safeguard against this?
* **Hint:** experiment with different bounds on the threshold value.



## Background correction
If you perform intensity measurement on your cells to quantify fluorescence intensity, it is important
that you subtract the background fluorescence that is present outside of your cells. If this background
is homogeneous (check by using line profile in ImageJ) and your cells are not too confluent you can try
the following strategy.

* Find the background in your primary objects image. 
    * Use `IdentifyPrimaryObjects` **without** discarding small or border objects to find your foreground objects
    * Dilate the found objects by a generous number of pixels. 
    * Mask the image that you want to correct using the `MaskImage` module. Use the dilated objects to mask the image and invert the mask.
* Use the `MeasureImageIntensity` module on the masked image. Only pixels in the background will contribute to the measurement.
* Use the `ImageMath` module to subtract the median intensity measurement from the previous step from your image. Using the median is more robust in case you did not correctly mask out all foreground objects.
* Inspect the intensities before and after applying `Image Math`. 

For correcting the intensity of an image channel that contains cytoplasm, follow the steps above, but additionally include `IdentifySecondaryObjects`.

## Filter out dead and mitotic cells - Quality Control

Cells have different morphologies depending on the stage of their cell cycle. If you look for drug-induced phenotypes in your cell-based measurements it is important that you only compare measurements during the same stage of the cell cycle.
In many cases you simply want to look at cells during interphase (this will of course depend on the particular assay). 
Try to remove dead and mitotic cells from your measurements.

* Look at the cell nucleus for a number of images. 
* How do dead and mitotic cells differ in shape and intensity?
* Using the `FilterObject` module try to remove dead and mitotic cells from the initial set of objects.

## Measure Image Quality to detect out of focus images - Quality Control
* Add the `MeasureImageQuality` module to the pipeline.
* Make sure the option `Calculate blur metrics` is checked.
* In the `ExportToSpreadsheet` module, make sure you include the image quality measurements in the output.
* Run the pipeline on an image set that contains some out-of-focus images.
* Inspect the `images.csv` file. Look for the column with the PowerLogLogSlope measurement. How does it differ between images that are in focus and images that are out of focus. Try to find a good cut-off value. 

Background:
The `power-log-log-slope` measurment gives you an indicatoin how well focused an image is:
<center><img src="../Screenshots/PLLS_Tischi.png" alt="" width=400px></center>


## Identify tertiary objects
* Place the `IdentifyTertiaryObjects` module in the pipeline and experiment with different settings. 
* When is this useful?

[Back to **Overview**](OverviewGettingStarted.md)


