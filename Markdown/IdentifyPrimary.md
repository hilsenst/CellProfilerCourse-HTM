# Segment Nuclei

Finding cell nuclei is typically the first segmentation step when analysing 
fluoresence images of cells. 
Nuclei are a good starting point for segmentation as they are typically contiguous objects which are 
spatially separated because they are surrounded by cytoplasm. If possible,
you should try to include a fluorescent marker for the nucleus in your assay.

# Experiment with the segmentation parameters

* Add the `IdentifyPrimaryObjects` module to the pipeline. Select the input image. ![](../Screenshots/IdentifyPrimaryObjects.png)
* Try running the module with the standard parameters. 
* Try using different segmentation methods (Otsu, Background, Robustbackground). ~self-study
* Set different size ranges. ~self-study
* Try different options for splitting objects. ~self-study 
* Go back to the `Smooth` module and try different Gaussian blur sizes. How does the smoothing change the segmentation results? ~self-study


[Proceed to **Segmenting Cytoplasm**](IdentifySecondary.md)  
[Back to **Overview**](OverviewGettingStarted.md)
