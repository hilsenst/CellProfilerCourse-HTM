# Segmenting Cytoplasm

Once we have identified the Nuclei, we can expand around them to find the cytoplasm.
In CellProfiler this is done using the `IdentifySecondaryObjects` module.

CellProfiler implements various methods to identify the cytoplasm. Which one to choose
depends largely on whether you have fluorescent marker for the cytoplasm or not. If
you want to accurately segment your cytoplasm your assay should include a fluorescent marker
that labels the whole cytoplasm or a cell delineation marker such as [CellMask](https://www.thermofisher.com/order/catalog/product/H32721) .

## Identify cytoplasm using dilation ~work-along
* add the `IdentifySecondaryObjects` module to the pipeline and select Nuclei as the primary objects
* If you do not have dedicated cytoplasm marker select `Distance-N`.
![](../Screenshots/IdentifySecondaryLine.png)
* Experiment with different numbers of pixels to expand. What happens if objects are close together? ~self-study
* Try and use the `Propagate` and `Watershed` methods using another channel as input image. ~self-study


**Caution:** Often, you can get a fairly good segmentation of the cytoplasm using a labelled protein that you are interested in
which localizes in the cytoplasm. While it is tempting to use this, you may bias your result in the following way: if a drug treatment changes 
the localization of your protein of interest, you may not longer accurately segment the cytoplasm.

[Proceed to **Take Measurements**](Measurements.md)

## Distraction: Experimenting with Distance-N? ~self-study

* Open a new CellProfiler project (save your old one)
* Drop the image MapOfEurope.png from the Screenshot folder onto the files box
* Create a pipeline comprising the two modules `IdentifyObjectsManually` and `IdentifySecondaryObjects` with `Distance-N`.
* Run test mode. In `IdentifyObjectsManually` mark the capitol cities.
* Run `IdentifySecondaryObjects` with various Distance-N parameters using the marked capital cities as primary objects. When do you run into neighbouring territories? What happens if you didn't mark all capital cities?
* Run the pipeline again, this time mark the geographical centre of each country. Do the results improve? Can you get you good approximation of the actual size (area) of each country just by marking the geographical centres and running Distance-N?
* How does this transfer to cells? Confluent vs. Non-confluent. Round vs. elongated shapes.
![](../Screenshots/DistanceN-Europe40.png)

[Proceed to **Take Measurements**](Measurements.md)  
[Back to **Overview**](OverviewGettingStarted.md)
