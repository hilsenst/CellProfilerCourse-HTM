# Image Preprocessing

The first step in most image analysis pipeline is some preprocessing of the raw
input images.
Use preprocessing when:

* images are noisy, particularly when using confocal data - use the `Smooth` module
* images have a strong homogeneous background and you want to take intensity measurments - use the `ImageMath` module. See [Additional Refinements](AdditionalMaterial.md) for how to automatically determine the background intensity.
* images are unevenly illuminated (for example strong vignetting) - use `CorrectIllumination`

## Gaussian smoothing to reduce noise ~work-along
* add the `Smooth` module
* select Gaussian smoothing
* try the smoothing filter with various artifact radii
![](../Screenshots/Smooth.png)

[Proceed to **Segmenting Nuclei**](IdentifyPrimary.md)  
[Back to **Overview**](OverviewGettingStarted.md)
