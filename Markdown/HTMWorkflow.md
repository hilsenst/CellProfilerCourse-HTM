# Where does CellProfiler fit into your high-throughput microscopy workflow 

## Cellprofiler helps us to transform images into measurements
<img src="../Screenshots/HTMPipeline.png" alt="" width=700px>

## Why not use something like ImageJ ?
There are many image analysis packages. Among biologists ImageJ is very popular,
so why not just create an ImageJ macro for your analysis? 
HTM image analysis requires more than just good image analysis operators.
Importantly HTM analysis requires:

* Pipeline building
* Batch Processing
* Metadata handling

### Pipeline building
A complex image analysis routine can consist of many individual image processing
and segmentation steps. ImageJ has many powerful macros, but they are often 
not designed to work together. In Cellprofiler you can build a pipeline from modules. 
The modules are designed to work together, that is you can easily re-use output images,
objects and measurements from one module in another module without using [*glue code*](https://en.wikipedia.org/wiki/Glue_code). 

### Batch Processing
Once you have established an image analysis pipeline, batch analysis of many images
in CellProfiler is easy. 

### Metadata handling
In a high throughput screening it is important to keep track of metadata that is
generated at various steps of the workflow. Cellprofiler helps you with some aspects
of the metadata handling, e.g. you can extract metadata from file names or the image
formats. The extracted metadata is then stored in the output tables together
with your image and object measurements. This facilitates linking back individual measurements
to different well positions (for heat maps), drug targets, reagents or simply to the original image
during the statistical analysis of your screening data.

<img src="../Screenshots/WorkflowMetadata.png" alt="" width=700px>

[Proceed to **Alternatives to Cellprofiler**](Alternatives.md)  
[Back to **Overview**](OverviewGettingStarted.md)



