# Using CellProfiler to analyze high throughput microscopy images.

## About
This git repository contains a tutorial on how to use Cellprofiler for analysing High-Throuput-Microscopy Data. 
The focus on using CellProfiler in a typical screening workflow. This tutorial is not meant to be exhaustive, but
rather to get you started.

For further self-study, there are plenty of test data sets, tutorials and tutorial videos for CellProfiler available online, 
many of them are linked to on the [CellProfiler Website](http://cellprofiler.org).

To get started with this tutorial proceed to [Overview and Getting Started](Markdown/OverviewGettingStarted.md)



Feel free to use this material for self-study. If you have suggestions for improvements or
want to use the material in your own courses, please contact me via email at
Volker Hilsenstein at embl dot de